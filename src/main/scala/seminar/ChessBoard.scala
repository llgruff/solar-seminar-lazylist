package seminar

import seminar.ChessBoard._

/**
 * https://ru.wikipedia.org/wiki/История_шахмат, см. «Трактат о шахматах»
 * Вычислите количество зерен пшеницы на шахматной доске,
 * учитывая, что число на каждом квадрате удваивается.
 * На шахматной доске 64 квадрата (1ый квадрат имеет 1 зерно, 2ой квадрат имеет 2 зерна и т.д.).
 */
sealed trait ChessBoard {
  def totalCount: BigInt
  def squareCount(idx: Int): Option[BigInt]
}

object ChessBoard {
  val ChessBoardSize: Int = 64
}

class ChessBoardLazy extends ChessBoard {
  override def totalCount: BigInt = {
    ???
  }

  override def squareCount(idx: Int): Option[BigInt] = {
    ???
  }
}

/**
 * Можно использовать побитовые сдвиги
 * https://elementy.ru/nauchno-populyarnaya_biblioteka/431670/Vsego_lish_stepeni_dvoyki
 */
class ChessBoardBinary extends ChessBoard {
  override def totalCount: BigInt = {
    ???
  }

  override def squareCount(idx: Int): Option[BigInt] = {
    ???
  }
}
