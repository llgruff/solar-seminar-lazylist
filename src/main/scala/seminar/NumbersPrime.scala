package seminar

import scala.annotation.tailrec

object NumbersPrime {

  /**
   * Получите список всех нечетных натуральных чисел
   */
  def odds: LazyList[BigInt] = {
    ???
  }

  /**
   * https://ru.wikipedia.org/wiki/Решето_Эратосфена —
   * алгоритм нахождения всех простых чисел.
   * По мере прохождения списка нужные числа остаются, а ненужные исключаются.
   * Простое число делится только на самого себя и 1.
   * 1 не является простым числом.
   */
  def primes: LazyList[BigInt] = {
    ???
  }

  /**
   * Вычислите простые множители натурального числа.
   */
  def primeFactors(number: BigInt): List[BigInt] = {
    ???
  }

}
