Курс программирования на языке Scala, 2022

Семинар 2022.03.30
Тема — Решаем задачи с использованием LazyList.

Посмотрите описание коллекции LazyList в [документации](https://www.scala-lang.org/api/current/scala/collection/immutable/LazyList.html).

Расписание всего курса + даты выхода лекций + ссылки на все видео и слайды можно найти [тут](https://maxcom.github.io/scala-course-2022/).

1. NumbersFibonacci — обсуждаем вместе 15-20 минут
2. NumbersPrime — 3 задачки по 5-10 минут
3. ChessBoard — 2 задачки по 10-20 минут
4. Terminator — 1 задача на 30-45 минут
5. Sudoku — обсуждаем +-30 минут
6. `*` Задача о ходе коня или Рыцарский тур [описание](https://en.wikipedia.org/wiki/Knight's_tour)  
   (для самостоятельного решения, по желанию)
